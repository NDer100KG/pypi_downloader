import sys
from subprocess import Popen, PIPE

if __name__ == "__main__":
    requirements = open("requirement.txt", "r")

    pkgs = requirements.readlines()

    for PKG_NAME in pkgs:
        if "\n" in PKG_NAME:
            PKG_NAME = PKG_NAME.replace("\n", "")
        
        command = "pip download " + PKG_NAME + "=="

        process = Popen(command, stdout = PIPE, stderr = PIPE, shell=True)
        output, error = process.communicate()

        str_start = error.find(b"from versions: ") + 15
        str_end = error.find(b")\bERROR:")

        all_version_available = error[str_start:str_end].decode("utf-8")
        all_version_available = all_version_available.split(",")

        for version in all_version_available:
            if " " in version:
                version  = version.replace(" ", "")
            
            try:
                Popen("pip download -d pkgs " + PKG_NAME + "=="+ version, shell = True).wait()

            except KeyboardInterrupt:
                sys.exit()