#!/bin/bash
conda create --name py35 python=3.5

conda create --name py36 python=3.6

conda create --name py37 python=3.7

conda create --name py38 python=3.8

conda create --name py27 python=2.7
