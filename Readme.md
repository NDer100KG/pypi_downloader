# pypi Auto downloader
pypi auto downloader using conda with py35-38 and requirement.txt

## Register gitlab runner
1. Create runner
    ```
    docker run -d --name gitlab-runner-pypi --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /home/nder/pypi_vol:/tmp \
    nder100kg/gitlab-runner:python
    ```
2. Register runner to repo
    ```
    docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
    ```   

