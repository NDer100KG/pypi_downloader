FROM gitlab/gitlab-runner:latest

USER root

RUN apt update && \
    apt install -y wget 

ENV PATH="/opt/miniconda3/bin:${PATH}"
ARG PATH="/opt/miniconda3/bin:${PATH}"
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 
RUN echo 'source ${HOME}/miniconda3/bin/activate' >> ~/.bashrc
RUN chown -R gitlab-runner:gitlab-runner /opt/miniconda3

## Install sudo
RUN apt-get install -y sudo \
    && echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers \
    && rm -rf /var/lib/apt/list/*
