docker build \
    -t nder100kg/gitlab-runner:conda \
    -f dockerfile \
    .

docker build \
    -t nder100kg/gitlab-runner:python \
    -f dockerfile-python \
    .
