mkdir pkgs

## Only install requirement
cat requirement.txt | sed -e '/^\s*#.*$/d' | xargs -n 1 pip download -d pkgs

## download all pkgs which can be fetched on pypi
# python all_version_downloader.py

echo "Copying pkgs/* to /tmp"
sudo cp pkgs/* /tmp